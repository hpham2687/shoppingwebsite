import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/app';
import 'firebase/firestore';
import * as firebase2 from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyA2vwiuT0aAzdqfWJsls9PJ-JIQ3Whr854',
  authDomain: 'molla-project.firebaseapp.com',
  databaseURL: 'https://molla-project.firebaseio.com',
  projectId: 'molla-project',
  storageBucket: 'molla-project.appspot.com',
  messagingSenderId: '868022771410',
  appId: '1:868022771410:web:b377995cb35311de10570b',
  measurementId: 'G-YCM2QSB2DN',
};
// Initialize Firebase
class FirebaseClass {
  constructor() {
    firebase.initializeApp(firebaseConfig);
    this.auth = firebase.auth();
    this.store = firebase.firestore();
  }

  // authentication
  createUserWithEmailAndPassword = (email, password) =>
    this.auth.createUserWithEmailAndPassword(email, password);

  signInWithEmailAndPassword = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

  doSignOut = () => {
    localStorage.removeItem('userInfo'); // store data

    return this.auth.signOut();
  };

  doPasswordReset = (email, actionCodeSettings) =>
    this.auth.sendPasswordResetEmail(email, actionCodeSettings);

  doPasswordUpdate = (password) => this.auth.currentUser.updatePassword(password);

  // store
  addDocument = (collectionName, data) => this.store.collection(collectionName).add(data);

  addDb = (collectionName, data) => {
    console.log(this.a);
    // this.a.forEach((data)=>{
    //   this.store.collection('productsData').add(data);

    // })
  };

  getAllItems = (collectionName) => this.store.collection(collectionName).get();

  deleteDocument = (collectionName, doc_id) =>
    this.store.collection(collectionName).doc(doc_id).delete();

  // user
  getUserInfo = async (uid) => {
    return await this.store
      .collection('usersData')
      .get()
      .then(function (querySnapshot) {
        let data = null;
        querySnapshot.forEach(function (doc) {
          if (doc.data().uid === uid) {
            data = doc.data();
          }
        });
        return data;
      })
      .catch(function (error) {
        console.log('Error getting documents: ', error);
      });
  };

  updateProductComment = async (doc_id, payload) => {
    const document = await this.store.collection('productsData').doc(doc_id);

    return document.update({
      comments: firebase.firestore.FieldValue.arrayUnion(payload),
    });
  };

  updateUserInfo = async (uid, data) => {
    console.log(uid, data);
    const _this = this;
    return await this.store
      .collection('usersData')
      .get()
      .then(function (querySnapshot) {
        return querySnapshot.forEach(function (doc) {
          if (doc.data().uid == uid) {
            return _this.store.collection('usersData').doc(doc.id).update(data);
          }
        });
      });
  };

  getCurrentUserOrders = async (uid) => {
    const _this = this;
    return await this.store
      .collection('usersData')
      .get()
      .then(function (querySnapshot) {
        return querySnapshot.forEach(function (doc) {
          if (doc.data().uid == uid) {
            return _this.store.collection('usersData').doc(doc.id);
          }
        });
      });
  };

  updateUserBillingAddress = async (uid, data) => {
    console.log(uid, data);
    const _this = this;
    return await this.store
      .collection('usersData')
      .get()
      .then(function (querySnapshot) {
        return querySnapshot.forEach(function (doc) {
          if (doc.data().uid == uid) {
            return _this.store.collection('usersData').doc(doc.id).update(data);
          }
        });
      });
  };

  updateUserPassWord = (newPassword = '12345') => {
    console.log(newPassword);
    const user = firebase.auth().currentUser;
    return user.updatePassword(newPassword);
  };
}

export default new FirebaseClass();
