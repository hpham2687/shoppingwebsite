import React, { useState } from 'react';
import { numberWithCommas, getCurrentDate } from '../../utils';
import { showNotification } from '../commons/Notification';
import * as uiActions from '../../actions/ui';

const MainCheckout = (props) => {
  const [shipFee, setShipFee] = useState(0);
  const { cartContexts, uiContexts, userContexts, userData, firebase } = props;

  const subTotal = () => {
    let sumSubTotal = 0;
    if (cartContexts.cart.length > 0)
      cartContexts.cart.forEach((value) => {
        sumSubTotal += value.count * value.doc_data.price;
      });
    return sumSubTotal;
  };

  const handleDeleteCartItem = (doc_id) => {
    cartContexts.cartDispatch({
      type: 'DELETE',
      payload: doc_id,
    });
    showNotification('success', 'Log out successfully!');
  };

  const handleChangeAmount = (value, e) => {
    let amount2Add = 0;
    const typeChange = e.currentTarget.dataset.name;
    if (typeChange === 'inc') amount2Add = 1;
    else {
      if (value.count === 1) handleDeleteCartItem(value.doc_id);
      amount2Add = -1;
    }
    cartContexts.cartDispatch({
      type: 'SET',
      payload: value,
      amount: amount2Add,
    });
  };

  const handleChangeShipType = (e) => {
    const shipType = e.currentTarget.dataset.name;
    switch (shipType) {
      case 'free-shipping':
        setShipFee(0);
        return;
      case 'standard-shipping':
        setShipFee(10);
        return;
      case 'express-shipping':
        setShipFee(20);

      default:
    }
  };

  const handleClickCheckOut = () => {
    // check login chua,
    if (!userData) {
      showNotification('success', 'Login first to order!');
      return uiContexts.UiDispatch(uiActions.setModalAuthStatus(true));
    }

    const data2Save = userData.orders ? userData.orders : [];
    cartContexts.cart.map((value) => {
      const {
        doc_data,
        doc_data: { name },
      } = value;
      const obj = {
        productName: name,
        date: getCurrentDate(),
        shipTo: userData.billingAddressInfo.billingAddress,
        saleAmount: doc_data.price,
        amount: value.count,
        status: 'Pending',
      };
      data2Save.push(obj);
    });
    firebase
      .updateUserBillingAddress(userData.uid, {
        orders: data2Save,
      })
      .then(async () => {
        showNotification('success', 'Check out successfully!');
        cartContexts.cartDispatch({ type: 'DELETE_ALL' });
        await reGetData();
        props.history.push('/');
      });
  };
  const reGetData = async () => {
    const userInfo = await firebase.getUserInfo(userData.uid);
    localStorage.setItem('userInfo', JSON.stringify(userInfo)); // store data
    userContexts.userDispatch({ type: 'SET', payload: userInfo });
  };
  const RenderCartList = () => {
    if (cartContexts.cart.length > 0)
      return cartContexts.cart.map((value, index) => {
        const { doc_id } = value;
        const { doc_data } = value;
        return (
          <tr key={index}>
            <td className="product-col">
              <div className="product">
                {/* <figure className="product-media">
                  <img src={convert2SmallImg(doc_data.pictures[0])} alt="Product" />
                </figure> */}
                <h3 className="product-title">{doc_data.name}</h3>
              </div>
            </td>
            <td className="price-col">
              {uiContexts.ui.currency === 'usd'
                ? `$${doc_data.price}`
                : `${numberWithCommas(doc_data.price * 22000)} đồng`}
            </td>
            <td className="quantity-col">
              <div className="cart-product-quantity-btn">
                <div
                  onClick={(e) => handleChangeAmount(value, e)}
                  data-name="dec"
                  className="input-group-prepend"
                >
                  <button
                    style={{ minWidth: '26px' }}
                    className="btn btn-decrement btn-spinner"
                    type="button"
                  >
                    <i className="fas fa-minus" />
                  </button>
                </div>
                <input
                  type="text"
                  style={{ textAlign: 'center' }}
                  className="form-control "
                  value={value.count}
                  required
                />
                <div
                  onClick={(e) => handleChangeAmount(value, e)}
                  data-name="inc"
                  className="input-group-append"
                >
                  <button
                    style={{ minWidth: '26px' }}
                    className="btn btn-increment btn-spinner"
                    type="button"
                  >
                    <i className="fas fa-plus" />
                  </button>
                </div>
              </div>
            </td>
            <td className="total-col">
              {uiContexts.ui.currency === 'usd'
                ? `$${value.count * doc_data.price}`
                : `${numberWithCommas(value.count * doc_data.price * 22000)} đồng`}
            </td>
            <td onClick={(e) => handleDeleteCartItem(doc_id)} className="remove-col">
              <i className="fal fa-times" />
            </td>
          </tr>
        );
      });
    return (
      <tr style={{ border: 'none' }}>
        <td>No Products in Cart!</td>
        <td />
        <td />
        <td />
        <td />
      </tr>
    );
  };
  return (
    <div className="checkout-containter">
      <div className="page-title-banner">
        <h3 className="page-title-banner___title">Shopping Cart</h3>
        <span className="page-title-banner___subtitle">SHOP</span>
      </div>

      <div className="checkout-content-wrapper">
        <div className="grid wide">
          <div className="row">
            <div className="col l-8 m-6 c-12">
              <table className="table-cart">
                <thead>
                  <tr>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
                    <th />
                  </tr>
                </thead>
                <tbody>
                  <RenderCartList />
                </tbody>
              </table>
            </div>
            <div className="col l-4 m-6 c-12">
              <div className="cart-overview">
                <h3 className="cart-overview__heading">Cart Total</h3>
                <table className="cart-overview__table">
                  <tr>
                    <th>Subtotal:</th>
                    <th>
                      {uiContexts.ui.currency === 'usd'
                        ? `$${subTotal()}`
                        : `${numberWithCommas(subTotal() * 22000)} đồng`}
                    </th>
                  </tr>
                  <tr>
                    <th>Shipping:</th>
                    <th />
                  </tr>
                  <tr>
                    <td>
                      <input
                        onChange={handleChangeShipType}
                        name="shipping"
                        type="radio"
                        data-name="free-shipping"
                        checked={shipFee === 0 ? 'true' : false}
                        className="custom-control-input"
                      />
                      Free shipping:
                    </td>
                    <td> $0.00</td>
                  </tr>
                  <tr>
                    <td>
                      <input
                        onChange={handleChangeShipType}
                        name="shipping"
                        type="radio"
                        data-name="standard-shipping"
                        checked={shipFee === 10 ? 'true' : false}
                        className="custom-control-input"
                      />
                      Standard:
                    </td>
                    <td>
                      {uiContexts.ui.currency === 'usd'
                        ? `$10.00`
                        : `${numberWithCommas(10 * 22000)} đồng`}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <input
                        onChange={handleChangeShipType}
                        name="shipping"
                        type="radio"
                        data-name="express-shipping"
                        checked={shipFee === 20 ? 'true' : false}
                        className="custom-control-input"
                      />
                      Express:
                    </td>
                    <td>
                      {uiContexts.ui.currency === 'usd'
                        ? `$20.00`
                        : `${numberWithCommas(20 * 22000)} đồng`}
                    </td>
                  </tr>
                </table>

                <div className="checkout-btn-group">
                  <div className="total-price">
                    <span>Total</span>
                    <span>
                      {uiContexts.ui.currency === 'usd'
                        ? `$${subTotal() + shipFee}`
                        : `${numberWithCommas((subTotal() + shipFee) * 22000)} đồng`}
                    </span>
                  </div>
                  <div
                    onClick={() => handleClickCheckOut()}
                    className="carousel__btn btn-outline-orange checkout-btn"
                  >
                    PROCEED TO CHECK OUT
                    <i className="fa fa-caret-right" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainCheckout;
