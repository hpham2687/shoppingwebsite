import React from 'react';

const AuthUserContext = React.createContext(null);
const CartContexts = React.createContext();
const ProductDataContexts = React.createContext();
const UserContexts = React.createContext();
const UiContexts = React.createContext();
const WishlistContexts = React.createContext();

export {
  WishlistContexts,
  AuthUserContext,
  CartContexts,
  ProductDataContexts,
  UserContexts,
  UiContexts,
};
