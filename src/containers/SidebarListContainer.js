import React, { useContext, useEffect, useState } from 'react';
import PriceSlider from '../components/MainSidebarList/PriceSlider';
import * as ContextApi from '../context';
import { getListCategory, getMaxPriceValue } from '../helpers';
import LeftSidebar from '../components/MainSidebarList/LeftSidebar';
import ListProduct from '../components/MainSidebarList/ListProduct';
import '../assets/css/SidebarList.scss';
import { FirebaseContext } from '../firebase';
import Spinner from '../components/commons/Spinner';

import * as uiActions from '../actions/ui';
import * as cartActions from '../actions/cart';
import { handleAddToWishList } from '../utils';

const SidebarListContainer = (props) => {
  const { ProductDataContexts, UiContexts, CartContexts, WishlistContexts } = ContextApi;
  const productDataContext = useContext(ProductDataContexts);
  const firebaseContext = useContext(FirebaseContext);
  const uiContexts = useContext(UiContexts);
  const cartContexts = useContext(CartContexts);
  const wishlistContext = useContext(WishlistContexts);

  const [pos, setPos] = useState(0);

  const [filterOptions, setFilterOptions] = useState({
    category: ['Furniture'],
    priceValue: 100,
  });
  const [isShowCategory, setShowCategory] = useState(true);
  const listCategory = getListCategory(productDataContext.productData);
  const handleAddToCart = ({ doc_id, doc_data }) => {
    uiContexts.UiDispatch(uiActions.setModalViewCartStatus(true));
    cartContexts.cartDispatch(cartActions.setCart({ doc_id, doc_data }));
  };

  const handleAddToWishLists = ({ doc_id, doc_data }) => {
    const numWishlish = handleAddToWishList({ doc_id, doc_data });
    wishlistContext.numWishlistDispatch({ type: 'SET_NUM_WISHLIST', payload: numWishlish });
  };

  const handleChangefilterCategoryOptions = (value) => {
    let filterCategoryOptions = [...filterOptions.category];

    if (filterCategoryOptions.indexOf(value) === -1) {
      filterCategoryOptions.push(value);
    } else {
      filterCategoryOptions = filterCategoryOptions.filter((el) => el != value);
    }
    setFilterOptions((prevState) => ({
      ...prevState,
      category: filterCategoryOptions,
    }));
  };

  const handleSelectOptions = (type) => {
    const filterCategoryOptions = [];

    if (type === 'All') listCategory.map((el) => filterCategoryOptions.push(el.name));

    setFilterOptions((prevState) => ({
      ...prevState,
      category: filterCategoryOptions,
    }));
  };

  const getAllProducts = async () => {
    return firebaseContext.getAllItems('productsData').then((querySnapshot) => {
      const listProducts = [];
      querySnapshot.forEach((doc) => {
        // console.log(`${doc.id} => ${doc.data()}`);
        listProducts.push({
          doc_id: doc.id,
          doc_data: doc.data(),
        });
      });

      productDataContext.productDataDispatch({
        type: 'SET',
        payload: listProducts,
      });
    });
  };

  useEffect(() => {
    getAllProducts();
  }, []);

  const handlePricingSlide = (e) => {
    setFilterOptions({ ...filterOptions, priceValue: e.target.value });
  };

  if (productDataContext.productData.length === 0) {
    return <Spinner />;
  }
  return (
    <>
      <div className="checkout-containter">
        <div className="page-title-banner">
          <h3 className="page-title-banner___title">List</h3>
          <span className="page-title-banner___subtitle">SHOP</span>
        </div>
        <div className="grid wide">
          <div className="wrapper-prod">
            <div className="row">
              <div className="col l-3">
                <div className="filter-bar">
                  <table id="table-sidebar">
                    <tbody>
                      <tr>
                        <th>Filters:</th>
                        <th>
                          <span
                            style={{ display: 'inline-block' }}
                            onClick={() => handleSelectOptions('All')}
                          >
                            Select All
                          </span>
                          <span onClick={() => handleSelectOptions('Clean')}>Clean all</span>
                        </th>
                      </tr>
                      <tr>
                        <th>Category:</th>
                        <th>
                          <i
                            onClick={() => {
                              const categoryMenuItems = document
                                .getElementById('table-sidebar')
                                .getElementsByClassName('category-menu-item');
                              const style = !isShowCategory ? 'table-row' : 'none';
                              for (let i = 0; i < categoryMenuItems.length; ++i) {
                                categoryMenuItems[i].style.display = style;
                              }
                              setShowCategory((prevState) => !prevState);
                            }}
                            className="fal fa-chevron-down btn-dropdown-category"
                          />
                        </th>
                      </tr>

                      <LeftSidebar
                        pos={pos}
                        setPos={setPos}
                        isShowCategory={isShowCategory}
                        productDataContext={productDataContext}
                        filterOptions={filterOptions}
                        handleChangefilterCategoryOptions={handleChangefilterCategoryOptions}
                      />

                      <tr>
                        <th>
                          <PriceSlider
                            min={10}
                            max={getMaxPriceValue(productDataContext.productData)}
                            handlePricingSlide={handlePricingSlide}
                            priceValue={filterOptions.priceValue}
                          />
                        </th>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div className="col l-9">
                <div className="filterd-product">
                  <ListProduct
                    pos={pos}
                    setPos={setPos}
                    handleAddToCart={handleAddToCart}
                    handleAddToWishList={handleAddToWishLists}
                    uiContexts={uiContexts}
                    cartContexts={cartContexts}
                    productDataContext={productDataContext}
                    filterOptions={filterOptions}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* <MainSidebarList
        uiContexts={uiContexts}
        cartContexts={cartContexts}
        productDataContext={productDataContext}
        filterOptions={filterOptions}
        {...props} */}
    </>
  );
};

export default SidebarListContainer;
