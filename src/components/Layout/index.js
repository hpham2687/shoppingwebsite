import React, { useContext } from 'react';
import Footer from '../commons/Footer';
import Header from '../commons/Header';
import ModalComponent from '../ModalComponent';
import { FirebaseContext } from '../../firebase';
import * as ContextApi from '../../context';

const Layout = (props) => {
  const firebase = useContext(FirebaseContext);
  const wishlistContext = useContext(ContextApi.WishlistContexts);

  return (
    <>
      <main className="main">
        <ModalComponent firebase={props.firebase} {...props} />
        <Header wishlistContext={wishlistContext} firebase={firebase} {...props} />

        {props.children}
        <Footer />
      </main>
    </>
  );
};

export default Layout;
