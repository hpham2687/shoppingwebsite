import React, { useContext } from 'react';
import $ from 'jquery';
import { Link } from 'react-router-dom';
import { showNotification } from '../commons/Notification';
import * as uiActions from '../../actions/ui';
import { UserContexts } from '../../context';

const ModalSideBarMenu = (props) => {
  const userContexts = useContext(UserContexts);

  const handleClickClose = () => {
    // $("#modal-sidebar").css("display", "none");
    props.uiContexts.UiDispatch(uiActions.setModalwModalSidebar(false));

    $('.modal-sidebar').addClass('modal-sidebar--hidden');
    $('.modal-sidebar-wrap').css('opacity', '0');
    $('.modal-sidebar-wrap').css('visibility', 'hidden');
  };

  const handleLogOut = () => {
    props.firebase
      .doSignOut()
      .then(() => {
        showNotification('success', 'Log out successfully!');
        handleClickClose();
      })
      .catch((error) => {
        showNotification('success', `Log out error!${error}`);
      });
  };
  console.log(userContexts.user);
  return (
    <>
      <div className="modal-sidebar-wrap modal-molla">
        <div className="modal-sidebar">
          <div>
            <div className="modal-sidebar__container">
              <i onClick={handleClickClose} className="fal fa-times" />
              <ul className="list-sidebar-menu">
                <li onClick={handleClickClose}>
                  <Link to="/account">MY ACCOUNT</Link>
                </li>
                <li onClick={handleClickClose}>
                  <Link to="/wishlist">MY WISHLIST</Link>
                </li>
                <li onClick={() => handleLogOut()}>sign out</li>
                <li>
                  welcome ,{' '}
                  {userContexts.user.authUser ? userContexts.user.authUser.displayName : 'guest'}
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="modal-molla__overlay" />
      </div>
    </>
  );
};

export default ModalSideBarMenu;
