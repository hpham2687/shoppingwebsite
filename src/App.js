import React, { Suspense, useReducer, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import './assets/css/custom.css';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';
import Spinner from './components/commons/Spinner';
import initState from './reducers/init';
import * as ContextApi from './context';
import * as reducer from './reducers';
import Firebase, { FirebaseContext } from './firebase';
import * as pages from './pages';
import * as PATH from './constants/path';
import NotFoundPage from './components/commons/NotFound';
import Layout from './components/Layout';
import PrivateRoute from './components/commons/ProtectedRoute';
import { countWishlish } from './utils';

function App() {
  const {
    ProductDataContexts,
    UserContexts,
    UiContexts,
    CartContexts,
    WishlistContexts,
  } = ContextApi;

  const [user, userDispatch] = useReducer(reducer.userReducer, {
    authUser: localStorage.getItem('userInfo')
      ? JSON.parse(localStorage.getItem('userInfo'))
      : initState.initialUserState,
  });
  const [numWishlist, numWishlistDispatch] = useReducer(
    reducer.wishlistReducer,
    countWishlish() || initState.initialWishlistState
  );

  const [productData, productDataDispatch] = useReducer(
    reducer.productDataReducer,
    initState.initialProductDataState
  );
  const [ui, UiDispatch] = useReducer(reducer.uiReducer, initState.initialUiState);

  const [cart, cartDispatch] = useReducer(
    reducer.cartReducer,
    JSON.parse(localStorage.getItem('cart')) || initState.initialCartState
  );

  useEffect(() => {
    Firebase.auth.onAuthStateChanged((authUser) => {
      if (authUser) {
        const fetchData = async () => {
          const userInfo = await Firebase.getUserInfo(authUser.uid);
          console.log(userInfo);
          localStorage.setItem('userInfo', JSON.stringify(userInfo)); // store data
          userDispatch({ type: 'SET', payload: userInfo });
          UiDispatch({ type: 'SET_MODAL_STATUS', status: false });
        };

        fetchData();
      } else {
        userDispatch({ type: 'SET', payload: null });
        localStorage.removeItem('userInfo'); // store data
      }
    });
  }, []);

  const propStaticComponent = {
    userContexts: { user, userDispatch },
    uiContexts: { ui, UiDispatch },
    productDataContext: { productData, productDataDispatch },
    cartContexts: { cart, cartDispatch },
  };

  return (
    <>
      <ToastContainer />
      <ProductDataContexts.Provider value={{ productData, productDataDispatch }}>
        <UserContexts.Provider value={{ user, userDispatch }}>
          <UiContexts.Provider value={{ ui, UiDispatch }}>
            <CartContexts.Provider value={{ cart, cartDispatch }}>
              <WishlistContexts.Provider value={{ numWishlist, numWishlistDispatch }}>
                <FirebaseContext.Provider value={Firebase}>
                  <Suspense fallback={<Spinner />}>
                    <Router>
                      <Layout firebase={Firebase} {...propStaticComponent}>
                        <Switch>
                          <Route exact path={PATH.HOME_PAGE} component={pages.HomePage} />
                          <Route exact path={PATH.CHECKOUT_PAGE} component={pages.CheckOutPage} />
                          <PrivateRoute
                            exact
                            path={PATH.ACCOUNT_PAGE}
                            component={pages.AccountPage}
                          />
                          <PrivateRoute exact path={PATH.ADMIN_PAGE} component={pages.AdminPage} />
                          <Route
                            exact
                            path={PATH.SIDEBAR_LIST_PAGE}
                            component={pages.SidebarListPage}
                          />
                          <Route
                            path={PATH.DETAIL_PRODUCT_PAGE}
                            component={pages.DetailProductPage}
                          />
                          <Route path={PATH.WISHLIST_PAGE} component={pages.WishlistPage} />

                          <Route component={NotFoundPage} />
                        </Switch>
                      </Layout>
                    </Router>
                  </Suspense>
                </FirebaseContext.Provider>
              </WishlistContexts.Provider>
            </CartContexts.Provider>
          </UiContexts.Provider>
        </UserContexts.Provider>
      </ProductDataContexts.Provider>
    </>
  );
}

export default App;
