import React, { useContext } from 'react';
import MainAccount from '../components/AccountComponents';
import { showNotification } from '../components/commons/Notification';
import * as ContextApi from '../context';
import { FirebaseContext } from '../firebase';

const AccountContainer = (props) => {
  const { ProductDataContexts, UserContexts, UiContexts, CartContexts } = ContextApi;

  const productDataContext = useContext(ProductDataContexts);
  const uiContexts = useContext(UiContexts);
  const cartContexts = useContext(CartContexts);
  const userContexts = useContext(UserContexts);
  const firebase = useContext(FirebaseContext);

  const handleLogOut = () => {
    firebase
      .doSignOut()
      .then(() => {
        showNotification('success', 'Log out successfully!');
      })
      .catch((error) => {
        showNotification('success', `Log out error!${error}`);
      });
  };

  const handleUpdateUserInfo = (uid, newData) => {
    firebase
      .updateUserInfo(uid, newData)
      .then(function () {
        showNotification('success', 'User info successfully updated!');
      })
      .catch(function (error) {
        showNotification('success', 'Error updating User info!');
      });
  };

  const handleUpdateUserPassword = (newPassword) => {
    firebase
      .updateUserPassWord(newPassword)
      .then(function () {
        showNotification('success', 'change pass success!');
      })
      .catch(function (error) {
        console.log(error);
        showNotification('success', 'change pass fail!');
      });
  };

  return (
    <>
      <MainAccount
        firebase={firebase}
        uiContexts={uiContexts}
        cartContexts={cartContexts}
        userContexts={userContexts}
        productDataContext={productDataContext}
        userData={userContexts.user.authUser}
        handleLogOut={handleLogOut}
        handleUpdateUserInfo={handleUpdateUserInfo}
        handleUpdateUserPassword={handleUpdateUserPassword}
        {...props}
      />
    </>
  );
};

export default AccountContainer;
