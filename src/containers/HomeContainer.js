import React, { useContext, useEffect } from 'react';
import MainHome from '../components/HomeComponents';
import * as ContextApi from '../context';
import { FirebaseContext } from '../firebase';
import { handleAddToWishList } from '../utils';

import * as cartActions from '../actions/cart';
import * as uiActions from '../actions/ui';

const HomeContainer = () => {
  const { ProductDataContexts, UiContexts, CartContexts, WishlistContexts } = ContextApi;

  const productDataContext = useContext(ProductDataContexts);
  const firebaseContext = useContext(FirebaseContext);
  const uiContexts = useContext(UiContexts);
  const cartContexts = useContext(CartContexts);
  const wishlistContext = useContext(WishlistContexts);

  const handleAddToCart = ({ doc_id, doc_data }) => {
    uiContexts.UiDispatch(uiActions.setModalViewCartStatus(true));
    cartContexts.cartDispatch(cartActions.setCart({ doc_id, doc_data }));
  };

  const handleAddToWishLists = ({ doc_id, doc_data }) => {
    const numWishlish = handleAddToWishList({ doc_id, doc_data });
    wishlistContext.numWishlistDispatch({ type: 'SET_NUM_WISHLIST', payload: numWishlish });
  };
  const getAllProducts = () => {
    firebaseContext.getAllItems('productsData').then((querySnapshot) => {
      const listProducts = [];
      querySnapshot.forEach((doc) => {
        listProducts.push({
          doc_id: doc.id,
          doc_data: doc.data(),
        });
      });

      //  console.log(listProducts);
      productDataContext.productDataDispatch({
        type: 'SET',
        payload: listProducts,
      });
    });
  };
  const handleClickTab = (e, type) => {
    console.log(type);
    console.log('vao day', e.currentTarget.dataset.name);
    if (type === 'TRENDY')
      uiContexts.UiDispatch(uiActions.setTrendyProductTab(e.currentTarget.dataset.name));
    else if (type === 'RECENT_ARRIVAL') {
      uiContexts.UiDispatch(uiActions.setRecentArrivalProductTab(e.currentTarget.dataset.name));
    }
  };

  useEffect(() => {
    // get list product
    getAllProducts();
  }, []);

  return (
    <>
      {ProductDataContexts.productData ? ProductDataContexts.productData : null}

      <MainHome
        handleAddToCart={handleAddToCart}
        handleAddToWishLists={handleAddToWishLists}
        handleClickTab={handleClickTab}
        uiContexts={uiContexts}
        cartContexts={cartContexts}
        wishlistContext={wishlistContext}
        productDataContext={productDataContext}
      />
    </>
  );
};

export default HomeContainer;
