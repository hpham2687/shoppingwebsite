import React from 'react';
import WishlistContainer from '../containers/WishlistContainer';

const WishlistPage = () => {
  return (
    <>
      <WishlistContainer />
    </>
  );
};

export default WishlistPage;
