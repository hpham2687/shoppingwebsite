export default {
  initialUserState: {
    authUser: null,
    isFetching: false,
  },
  initialProductDataState: [],
  initialUiState: {
    isShowModalSidebar: false,
    isShowModalAuth: false,
    isShowModalResetPassword: false,
    isShowModalViewCart: false,
    trendyProductTab: 'All',
    recentArrivalProductTab: 'All',
    currency: 'usd',
  },

  initialCartState: [],
  initialWishlistState: 0,
};
