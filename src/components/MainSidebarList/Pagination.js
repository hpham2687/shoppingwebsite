import React from 'react';
import { scroller } from 'react-scroll';

function Pagination(props) {
  const { aclass, count, unit = 5, pos, setPos } = props;

  const pageCount = parseInt(count / unit) + (count % unit > 0 ? 1 : 0);
  console.log(pageCount);
  const pageNumbers = [];

  for (let i = 0; i < pageCount; i++) {
    pageNumbers.push(i + 1);
  }

  const ScrollToTopListProduct = () => {
    scroller.scrollTo('myScrollToListProduct', {
      duration: 1000,
      delay: 100,
      smooth: true,
      offset: 50, // Scrolls to element + 50 pixels down the page
    });
  };
  function onPageLink(e, index) {
    setPos(index - 1);
    ScrollToTopListProduct();
  }

  function onPrev(e) {
    if (pos === 0) return;
    setPos(pos - 1);
    ScrollToTopListProduct();
  }

  function onNext(e) {
    if (pos === pageCount - 1) return;

    setPos(pos + 1);
    ScrollToTopListProduct();
  }

  return (
    <nav aria-label="Page navigation" style={{ display: count === 0 ? 'none' : '' }}>
      <ul className={`pagination ${aclass}`}>
        <li className={`page-item ${pos === 0 ? 'page-link--disabled' : ''}`}>
          <button
            className="page-link-prev"
            to="#"
            aria-label="Previous"
            tabIndex="-1"
            aria-disabled="true"
            onClick={onPrev}
          >
            <span aria-hidden="true">
              <i className="fal fa-long-arrow-left" />{' '}
            </span>
            Prev
          </button>
        </li>

        {pageNumbers.map((item, index) => (
          <li
            className={`page-item ${item - 1 === pos ? 'page-link--active' : ''}`}
            aria-current="page"
            key={index}
          >
            <button className="page-link" to="#" onClick={(e) => onPageLink(e, item)}>
              {item}
            </button>
          </li>
        ))}

        {pageCount > 3 && pos < pageCount ? (
          <li className="page-item-total">of {pageCount}</li>
        ) : (
          ''
        )}

        <li className={`page-item ${pageCount - 1 === pos ? 'page-link--disabled' : ''}`}>
          <button className="page-link-next" to="#" aria-label="Next" onClick={onNext}>
            Next{' '}
            <span aria-hidden="true">
              <i className="fal fa-long-arrow-right" />{' '}
            </span>
          </button>
        </li>
      </ul>
    </nav>
  );
}

export default Pagination;
