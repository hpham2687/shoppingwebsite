export const HOME_PAGE = '/';
export const CHECKOUT_PAGE = '/checkout';
export const ACCOUNT_PAGE = '/account';
export const DETAIL_PRODUCT_PAGE = '/detail/:id';
export const ADMIN_PAGE = '/admin';
export const SIDEBAR_LIST_PAGE = '/sidebar-list';
export const WISHLIST_PAGE = '/wishlist';
