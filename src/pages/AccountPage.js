import React from "react";
import AccountContainer from "../containers/AccountContainer";

const AccountPage = (props) => {
  return (
    <>
      <AccountContainer {...props} />
    </>
  );
};

export default AccountPage;
