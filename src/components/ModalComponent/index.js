import React from 'react';
import ModalLogin from './ModalLogin';
import ModalSideBarMenu from './ModalSideBarMenu';
import ModalViewCart from './ModalViewCart';
import ModalResetPassword from './ModalResetPassword';

const ModalComponent = (props) => {
  const { isShowModalViewCart, isShowModalAuth, isShowModalSidebar } = props.uiContexts.ui;
  return (
    <>
      {(isShowModalViewCart || isShowModalAuth || isShowModalSidebar) && (
        <div className="overlay-modal" />
      )}
      <ModalViewCart {...props} />
      <ModalLogin {...props} />
      <ModalResetPassword {...props} />

      <ModalSideBarMenu firebase={props.firebase} {...props} />
    </>
  );
};

export default ModalComponent;
