import React, { useEffect } from "react";

const PriceSlider = (props) => {
  const { priceValue, handlePricingSlide, min, max } = props;

  const slider = React.createRef();
  useEffect(() => {
    console.log(priceValue);
  });
  return (
    <>
      {priceValue} - {max}
      <div className="price-slider-container">
        <input
          type="range"
          step="1"
          min={min}
          max={max}
          defaultValue={min}
          value={priceValue}
          onChange={handlePricingSlide}
          ref={slider}
          className="price-slider"
        />
      </div>
    </>
  );
};

export default PriceSlider;
