import React from "react";
import { Link } from "react-router-dom";
const NotFound = () => {
  return (
    <>
      <div
        className="notfound-wrapper"
        style={{
          backgroundImage:
            'url("https://d-themes.com/react/molla/assets/images/backgrounds/error-bg.jpg")',
        }}
      >
        <h1 className="error-title">Error 404</h1>
        <p>We are sorry, the page you've requested is not available.</p>
        <div class="carousel__btn btn-outline-orange ">
          {" "}
          <Link to="/">BACK TO HOME</Link>
        </div>
      </div>
    </>
  );
};

export default NotFound;
