import React, { useContext } from 'react';
import MainCheckout from '../components/CheckoutComponents';
import * as ContextApi from '../context';
import { FirebaseContext } from '../firebase';

const CheckoutContainer = (props) => {
  const { ProductDataContexts, UiContexts, CartContexts, UserContexts } = ContextApi;

  const productDataContext = useContext(ProductDataContexts);
  const uiContexts = useContext(UiContexts);
  const cartContexts = useContext(CartContexts);
  const userContexts = useContext(UserContexts);
  const firebase = useContext(FirebaseContext);

  return (
    <>
      <MainCheckout
        firebase={firebase}
        uiContexts={uiContexts}
        cartContexts={cartContexts}
        productDataContext={productDataContext}
        userContexts={userContexts}
        userData={userContexts.user.authUser}
        {...props}
      />
    </>
  );
};

export default CheckoutContainer;
