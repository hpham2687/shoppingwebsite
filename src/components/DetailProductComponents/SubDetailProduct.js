import React, { useState, useRef } from 'react';
import { Animated } from 'react-animated-css';
import Skeleton from 'react-loading-skeleton';
import { convert2SmallImg, numberWithCommas } from '../../utils';
import { showNotification } from '../commons/Notification';

const MainDetailProduct = (props) => {
  const { choosingProduct, uiContexts } = props;
  const [indexImg, setIndexImg] = useState(0);
  const [isShowAnimation, setIsShowAnimation] = useState(true);
  const [amount2Add, setAmount2Add] = useState(1);
  const [isShowModal, setIsShowModal] = useState(false);
  const imgDiv = useRef();
  const imageList = choosingProduct ? choosingProduct.doc_data.pictures : null;
  const CategoryArray = choosingProduct ? choosingProduct.doc_data.category : null;

  const handleMouseMove = (e) => {
    const { left, top, width, height } = e.target.getBoundingClientRect();
    const x = ((e.pageX - left) / width) * 100;
    const y = ((e.pageY - top) / height) * 100;
    imgDiv.current.style.backgroundPosition = `${x}% ${y}%`;
  };

  const handleClickPrevious = () => {
    setIsShowAnimation(false);
    if (indexImg > 0) setIndexImg((prevState) => prevState - 1);
    if (indexImg === 0) setIndexImg(imageList.length - 1);
    setInterval(() => {
      setIsShowAnimation(true);
    }, 1000);
  };
  const handleClickFollowing = () => {
    setIsShowAnimation(false);
    if (indexImg < imageList.length - 1) setIndexImg((prevState) => prevState + 1);
    if (indexImg === imageList.length - 1) setIndexImg(0);
    if (indexImg === 0) setIndexImg(imageList.length - 1);
    setInterval(() => {
      setIsShowAnimation(true);
    }, 1000);
  };

  const renderModalShowFullScreenImg = () => {
    if (isShowModal)
      return (
        <>
          <div className="modal-show-fullscreen-img">
            <div className="modal-show-fullscreen-img__content">
              <div className="modal-show-fullscreen-img-center">
                <Animated
                  animationIn="bounceInLeft"
                  animationOut="bounceOutRight"
                  isVisible={isShowAnimation}
                >
                  <img src={convert2SmallImg(imageList[indexImg])} alt="big imgs" />
                </Animated>
              </div>
              <div className="modal-show-fullscreen-img__toolbar">
                <i className="fas fa-search-plus" />
                <i className="fas fa-search-minus" />
                <i onClick={() => setIsShowModal(false)} className="fas fa-times" />
              </div>
              <div className="modal-show-fullscreen-img__navigator">
                <i onClick={handleClickPrevious} className="fas fa-chevron-left" />
                <i onClick={handleClickFollowing} className="fas fa-chevron-right" />
              </div>
            </div>
            <div className="modal-show-fullscreen-img__overlay" />
          </div>
        </>
      );
    return null;
  };
  const handleChangeAmount = (e) => {
    const typeChange = e.currentTarget.dataset.name;
    if (typeChange === 'inc') setAmount2Add((prevState) => prevState + 1);
    else {
      setAmount2Add((prevState) => prevState - 1);
    }
  };

  const handleAddToCart = ({ doc_id, doc_data }) => {
    props.cartContexts.cartDispatch({
      type: 'SET',
      payload: { doc_id, doc_data },
      amount: amount2Add,
    });
  };

  const placeholder = {
    imageList: () => {
      const arr = [1, 2, 3, 4];
      return arr.map((value) => (
        <Skeleton className="imageListItem" height={100} width={100} key={value} />
      ));
    },
    bigPreview: () => {
      return <Skeleton className="bigPreview-skeleton" height={450} width={418} />;
    },
    stars: () => {
      return <Skeleton className="stars-skeleton" height={12} width={14} />;
    },
    price: () => {
      return <Skeleton className="price-skeleton" height={12} width={14} />;
    },
    shortDesc: () => {
      return <Skeleton className="shortDesc-skeleton" height={83} width={530} />;
    },
    category: () => {
      return <Skeleton className="category-skeleton" />;
    },
  };
  return (
    <>
      {renderModalShowFullScreenImg()}
      <div id="list-preview" className="row">
        <div className="col l-15">
          <div className="detail-page__small-preview">
            {imageList
              ? imageList.map((value, index) => {
                  return (
                    <img
                      key={index}
                      onClick={() => setIndexImg(index)}
                      src={convert2SmallImg(value)}
                      className={indexImg === index ? 'detail-page__small-preview--active' : null}
                      alt="product back"
                    />
                  );
                })
              : placeholder.imageList()}
          </div>
        </div>
        <div
          style={imageList ? { border: '1px solid #999' } : { border: 'none' }}
          className="col l-4"
          id="big-prv"
        >
          {imageList ? (
            <div
              className="detail-page__big-preview"
              ref={imgDiv}
              onMouseLeave={() => (imgDiv.current.style.backgroundPosition = `center`)}
              onMouseMove={handleMouseMove}
              style={{
                backgroundImage: `url(${convert2SmallImg(imageList[indexImg])})`,
                backgroundSize: 'cover',
              }}
            >
              <div onClick={() => setIsShowModal(true)} className="show-fullscreen-img-btn">
                <i className="fal fa-arrows-alt" />
              </div>
            </div>
          ) : (
            placeholder.bigPreview()
          )}
        </div>
        <div className="col l-55">
          <div className="detail-content-container">
            <h3>{choosingProduct ? choosingProduct.doc_data.name : <Skeleton />}</h3>
            <div className="star">
              <i className="fa fa-star active-star" />
              <i className="fa fa-star active-star" />
              <i className="fa fa-star" />
              <i className="fa fa-star" />
              <i className="fa fa-star" />
              <span className="num-review">(2 Reviews)</span>
            </div>
            <span className="detail-price">
              {uiContexts.ui.currency === 'usd'
                ? `$${choosingProduct ? choosingProduct.doc_data.price : 200}`
                : `${numberWithCommas(
                    (choosingProduct ? choosingProduct.doc_data.price : 200) * 22000
                  )} đồng`}
            </span>

            {choosingProduct ? (
              <p>{choosingProduct.doc_data.shortDesc} </p>
            ) : (
              placeholder.shortDesc()
            )}

            <div className="quanity-group">
              <span>Qty</span>
              <div className="cart-product-quantity-btn --detail-page">
                <div onClick={handleChangeAmount} data-name="dec" className="input-group-prepend">
                  <button
                    className="btn btn-decrement btn-spinner"
                    type="button"
                    style={{ minWidth: '26px' }}
                  >
                    <i className="fas fa-minus" />
                  </button>
                </div>
                <input
                  type="text"
                  className="form-control "
                  disabled
                  value={amount2Add}
                  style={{ textAlign: 'center' }}
                />
                <div onClick={handleChangeAmount} data-name="inc" className="input-group-append">
                  <button className="btn btn-increment btn-spinner" type="button">
                    <i className="fas fa-plus" />
                  </button>
                </div>
              </div>
            </div>
            <div
              onClick={() => handleAddToCart(choosingProduct)}
              className="carousel__btn btn-outline-orange checkout-btn --checkout-btn-page "
            >
              <i style={{ marginRight: '10px' }} className="far fa-cart-plus" />
              ADD TO CART
            </div>
            <div className="detail-content-container__bottom">
              <span>
                Category:{' '}
                {CategoryArray
                  ? CategoryArray.map((val, i) => {
                      if (CategoryArray.length > 1 && i !== CategoryArray.length - 1) {
                        return `${val}, `;
                      }
                      return val;
                    })
                  : placeholder.category()}
              </span>
              <div className="social-share-icons">
                <span>Share:</span>
                {['facebook-f', 'twitter', 'pinterest', 'facebook'].map((value) => (
                  <a href="/" className="social-icon">
                    <i className={`fab fa-${value}`} />
                  </a>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default MainDetailProduct;
