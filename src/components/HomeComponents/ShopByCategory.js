import React from 'react';
import { useTranslation } from 'react-i18next';
import { Element } from 'react-scroll';
import LazyLoad from 'react-lazyload';
import { Link } from 'react-router-dom';

const ShopByCategory = () => {
  const { t, i18n } = useTranslation();

  return (
    <>
      <div className="trendy-product">
        <Element name="myScrollToShopByCategory">
          <div className="grid wide">
            <h1 className="trendy-product__title">{t('shop-by-cart.title')}</h1>
            <div className="product-by-cate-area">
              <div className="row">
                <div className="col l-4 m-6 c-12">
                  <div className="product-by-cate-area-item">
                    <div className="intro-overlay" />
                    <h3 className="product-by-cate-area-item__title">{t('shop-by-cart.title')}</h3>
                    <div className="product-by-cate-area-item__btn carousel__btn btn-outline-white">
                      <Link style={{ textDecoration: 'none', color: 'white' }} to="/sidebar-list">
                        SHOP NOW
                      </Link>
                      <i className="fa fa-caret-right" />
                    </div>
                    <div className="product-by-cate-area-item-background">
                      <LazyLoad height={200} offset={100}>
                        <img
                          src="https://d-themes.com/react/molla/demo-2/assets/images/home/banners/banner-1.jpg"
                          alt=""
                        />
                      </LazyLoad>
                    </div>
                  </div>
                </div>
                <div className="col l-4 m-6 c-12 col-list-item">
                  <div className="product-by-cate-area-item">
                    <div className="intro-overlay" />
                    <h3 className="product-by-cate-area-item__title">{t('shop-by-cart.fandd')}</h3>
                    <div className="product-by-cate-area-item__btn carousel__btn btn-outline-white">
                      <Link style={{ textDecoration: 'none', color: 'white' }} to="/sidebar-list">
                        SHOP NOW
                      </Link>
                      <i className="fa fa-caret-right" />
                    </div>
                    <div className="product-by-cate-area-item-background">
                      <LazyLoad height={200} offset={100}>
                        <img
                          src="https://d-themes.com/react/molla/demo-2/assets/images/home/banners/banner-2.jpg"
                          alt=""
                        />
                      </LazyLoad>
                    </div>
                  </div>
                  <div className="product-by-cate-area-item">
                    <div className="intro-overlay" />
                    <h3 className="product-by-cate-area-item__title">{t('shop-by-cart.kal')}</h3>
                    <div className="product-by-cate-area-item__btn carousel__btn btn-outline-white">
                      <Link style={{ textDecoration: 'none', color: 'white' }} to="/sidebar-list">
                        SHOP NOW
                      </Link>
                      <i className="fa fa-caret-right" />
                    </div>
                    <div className="product-by-cate-area-item-background">
                      <LazyLoad height={200} offset={100}>
                        <img
                          src="https://d-themes.com/react/molla/demo-2/assets/images/home/banners/banner-3.jpg"
                          alt=""
                        />
                      </LazyLoad>
                    </div>
                  </div>
                </div>
                <div className="col l-4 m-6 c-12">
                  <div className="product-by-cate-area-item">
                    <div className="intro-overlay" />
                    <h3 className="product-by-cate-area-item__title">
                      {' '}
                      {t('shop-by-cart.lighting')}
                    </h3>
                    <div className="product-by-cate-area-item__btn carousel__btn btn-outline-white">
                      <Link style={{ textDecoration: 'none', color: 'white' }} to="/sidebar-list">
                        SHOP NOW
                      </Link>
                    </div>
                    <div className="product-by-cate-area-item-background">
                      <LazyLoad height={200} offset={100}>
                        <img
                          src="https://d-themes.com/react/molla/demo-2/assets/images/home/banners/banner-4.jpg"
                          alt=""
                        />
                      </LazyLoad>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Element>
      </div>
    </>
  );
};

export default ShopByCategory;
