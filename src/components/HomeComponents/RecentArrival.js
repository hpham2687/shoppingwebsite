import React, { useState, useEffect } from 'react';
import { CSSTransition } from 'react-transition-group';
// ES6 Imports
import * as Scroll from 'react-scroll';
import { useTranslation } from 'react-i18next';
import { filterProduct, isInWishlish } from '../../utils';
import Product from '../Product';
import Spinner from '../commons/Spinner';

const { Element, scroller } = Scroll;

const RecentArrival = (props) => {
  const {
    uiContexts,
    productDataContext: { productData },
    handleAddToWishLists,
    handleAddToCart,
    handleClickTab,
  } = props;
  const activeTab = uiContexts.ui.recentArrivalProductTab;

  const [numProduct2Show, setNumProduct2Show] = useState(3);
  const [loading, setLoading] = useState(false);
  const [isShowing, setIsShowing] = useState(false);
  const { t } = useTranslation();

  // const numProductData = productData.length;

  const numProductData =
    filterProduct(productData, {
      activeTab,
      filterCategories: ['newArrival'],
    }).length - 1;

  useEffect(() => {
    if (numProductData > 0) setIsShowing(true);
    const intervalId = setInterval(() => {
      setIsShowing(false);
    }, 1000);
    return () => {
      clearInterval(intervalId);
    };
  }, [activeTab]);

  const handleClickLoadingMore = () => {
    setLoading(true);
    setIsShowing(true);
    console.log(`${numProduct2Show}-${numProductData}`);
    if (numProduct2Show === numProductData) {
      setNumProduct2Show(3);
    } else if (numProduct2Show + 4 > numProductData) {
      //  console.log('vao 1');
      setNumProduct2Show(numProductData);
    } else if (numProduct2Show + 4 <= numProductData) {
      // console.log('vao 2');

      setNumProduct2Show((prevState) => prevState + 4);
    }
    scroller.scrollTo('myScrollToRecentArrivalProduct', {
      duration: 1500,
      delay: 100,
      smooth: true,
      offset: numProduct2Show >= numProductData ? 0 : 413 * (numProduct2Show / 4), // Scrolls to element + 50 pixels down the page
    });
    const intervalId = setInterval(() => {
      setLoading(false);
      setIsShowing(false);
    }, 1000);
    return () => {
      clearInterval(intervalId);
    };
  };

  const RenderStar = ({ numStar }) => {
    return [...Array(5)].map((value, index) => (
      <i key={index} className={`fa fa-star ${index + 1 <= numStar ? 'active-star' : null}`} />
    ));
  };

  const ListRecentArrivalProducts = () => {
    if (numProductData > 0)
      return filterProduct(productData, {
        activeTab,
        filterCategories: ['newArrival'],
      }).map((value, index) => {
        if (index > numProduct2Show) return null; // chỉ hiện thị 4 phần tử
        const { doc_id } = value;
        const { doc_data } = value;

        return (
          <div key={index} className="col l-3 m-4 c-6">
            <Product
              length={productData.length}
              doc_id={doc_id}
              doc_data={doc_data}
              isInWishlish={isInWishlish(doc_id)}
              handleAddToWishList={handleAddToWishLists}
              handleAddToCart={handleAddToCart}
              uiContexts={uiContexts}
              RenderStar={RenderStar}
            />
          </div>
        );
      });
    return null;
  };
  const renderTab = () => {
    const listTab = ['All', 'Furniture', 'Decoration', 'Lighting'];
    return listTab.map((value, index) => (
      <li
        key={index}
        onClick={(e) => {
          handleClickTab(e, 'RECENT_ARRIVAL');
          setNumProduct2Show(3);
        }}
        data-name={value}
        className={activeTab === value ? 'active-li showing' : null}
      >
        {t(`product-category.${value.toUpperCase()}`)}
      </li>
    ));
  };
  return (
    <>
      <div className="trendy-product">
        <Element name="myScrollToRecentArrivalProduct">
          <div className="grid wide">
            <h1 className="trendy-product__title">{t('recent-arrival.title')}</h1>
            <ul className="trendy-product__menu">{renderTab()}</ul>

            {productData.length > 0 ? (
              <CSSTransition in={isShowing} classNames="product-animation" timeout={1000}>
                <div className="product-area">
                  <div className="row">
                    <ListRecentArrivalProducts />
                  </div>
                </div>
              </CSSTransition>
            ) : (
              <Spinner />
            )}

            <div className="more-container">
              <div onClick={handleClickLoadingMore} className="load-more-btn btn-white">
                {t('product-category.load-more-btn')}
                {!loading ? (
                  numProduct2Show === numProductData ? (
                    <i className="fa fa-long-arrow-up" />
                  ) : (
                    <i className="fa fa-long-arrow-down" />
                  )
                ) : (
                  <i className="fas fa-spinner" />
                )}
              </div>
            </div>
          </div>
        </Element>
      </div>
    </>
  );
};

export default RecentArrival;
