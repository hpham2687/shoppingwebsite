import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { convert2SmallImg, numberWithCommas } from '../../utils';

const Product = (props) => {
  const { t } = useTranslation();

  const {
    doc_id,
    doc_data,
    handleAddToCart,
    RenderStar,
    uiContexts,
    handleAddToWishList,
    isInWishlish,
  } = props;
  const numReview = doc_data.reviews || 0;
  const numStar = doc_data.ratings;
  const isSale = doc_data.discount;
  // console.log(isInWishlish);
  return (
    <>
      <div className="product-item" style={{ width: '282px' }}>
        <div className={`product-label product-label--${isSale ? 'green' : 'red'}`}>
          {isSale ? `Sale ${doc_data.discount}%` : 'TRENDY'}
        </div>
        <div
          onClick={() => handleAddToWishList({ doc_id, doc_data })}
          className="add-to-wishlish-icon"
          style={isInWishlish ? { background: '#c96', color: 'white' } : null}
        >
          <i className="far fa-heart" />
        </div>
        <div
          onClick={() => handleAddToCart({ doc_id, doc_data })}
          className="pc btn-white add-to-cart-btn"
        >
          Add to cart
        </div>
        <img src={convert2SmallImg(doc_data.pictures[0])} alt="" className="product-image" />
        {doc_data.pictures.length > 1 ? (
          <img
            src={convert2SmallImg(doc_data.pictures[1])}
            alt="Product"
            className="product-image-hover"
          />
        ) : (
          <img
            src={convert2SmallImg(doc_data.pictures[0])}
            alt="Product"
            className="product-image-hover"
          />
        )}

        <Link to={`/detail/${doc_id}`}>
          <div className="brief-describe">
            <span className="type">
              {t(`product-category.${doc_data.category[0].toUpperCase()}`)}
            </span>
            <h3 className="brief-describe__titile-price">{doc_data.name}</h3>
            <h3 className="brief-describe__titile-price">
              {uiContexts.ui.currency === 'usd'
                ? `$${doc_data.price}`
                : `${numberWithCommas(doc_data.price * 22000)} đồng`}
            </h3>
            <div className="star">
              <RenderStar numStar={numStar} />
              <span className="num-review">({numReview} Reviews)</span>
            </div>
          </div>
        </Link>
        <div
          onClick={() => handleAddToCart({ doc_id, doc_data })}
          className="mobile-flex bot add-to-cart-btn-product-item"
        >
          <i style={{ marginRight: '10px' }} className="far fa-cart-plus" /> Add to cart
        </div>
      </div>
    </>
  );
};

export default Product;
