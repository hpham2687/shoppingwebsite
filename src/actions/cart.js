export const cartActionTypes = {
  SET: 'SET',
  DELETE: 'DELETE',
  DELETE_ALL: 'DELETE_ALL',
};

export const setCart = (payload) => {
  return {
    type: cartActionTypes.SET,
    payload,
  };
};

export const loginUser = (user, remember) => (dispatch) => {
  return dispatch({ type: cartActionTypes.LOGIN, user, remember });
};

export const deleteCart = () => (dispatch) => {
  return dispatch({ type: cartActionTypes.REMOVE });
};

export const deleteAllCart = () => (dispatch) => {
  alert('dsfsf');
  return dispatch({ type: cartActionTypes.DELETE_ALL });
};
