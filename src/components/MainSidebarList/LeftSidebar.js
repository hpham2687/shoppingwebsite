import React from 'react';
import { Redirect } from 'react-router-dom';
import Slide from 'react-reveal/Slide';
import { getListCategory } from '../../helpers';

const LeftSideBar = (props) => {
  const { setPos } = props;
  const filterCategoryOptions = props.filterOptions.category
    ? props.filterOptions.category
    : 'Decor';

  const data = props.productDataContext.productData;
  if (!data) return <Redirect to="/" />;
  const listCategory = getListCategory(data);
  return listCategory.map((el, index) => {
    return (
      <Slide distance="10%" duration={1000} left>
        <tr className="category-menu-item" key={index}>
          <th>
            <div className="filter-checkbox">
              <input
                type="checkbox"
                className="custom-control-input"
                id={`id${el.name}`}
                onClick={(e) => {
                  props.handleChangefilterCategoryOptions(e.target.name);
                  setPos(0);
                }}
                name={el.name}
                checked={filterCategoryOptions.includes(el.name)}
              />
              <span
                id={`${el.name}`}
                name={el.name}
                onClick={(e) => {
                  props.handleChangefilterCategoryOptions(e.target.id);
                  setPos(0);
                }}
                className="checkmark"
              />

              <label className="custom-control-label" htmlFor={`id${el.name}`}>
                {el.name}
              </label>
            </div>
          </th>
          <th>
            <span className="category-amount">{el.count}</span>
          </th>
        </tr>
      </Slide>
    );
  });
};

export default LeftSideBar;
