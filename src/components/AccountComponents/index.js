import React, { useState } from 'react';
import { Formik } from 'formik';
import Reveal from 'react-reveal/Reveal';
import { Link } from 'react-router-dom';
import Pagination from '../MainSidebarList/Pagination';

const validate = (values) => {
  const errors = {};
  if (!values.firstName) {
    errors.firstName = 'Required';
  } else if (values.firstName.length < 3) {
    errors.firstName = 'Must be 3 characters or more';
  }

  if (!values.lastName) {
    errors.lastName = 'Required';
  } else if (values.lastName.length < 3) {
    errors.lastName = 'Must be 3 characters or more';
  }
  if (!values.displayName) {
    errors.displayName = 'Required';
  } else if (values.displayName.length < 10) {
    errors.displayName = 'Must be 10 characters or more';
  }

  if (values.currentPassword) {
    if (!values.currentPassword) {
      errors.currentPassword = 'Required';
    } else if (values.currentPassword.length < 6) {
      errors.currentPassword = 'Must be 6 characters or more';
    }

    if (!values.newPassword) {
      errors.newPassword = 'Required';
    } else if (values.newPassword.length < 6) {
      errors.newPassword = 'Must be 6 characters or more';
    }

    if (!values.confirmPassword) {
      errors.confirmPassword = 'Required';
    } else if (values.confirmPassword.length < 6) {
      errors.confirmPassword = 'Must be 6 characters or more';
    } else if (values.confirmPassword !== values.newPassword) {
      errors.confirmPassword = 'Not Match';
    }
  }

  return errors;
};

const MainAccount = (props) => {
  const {
    handleLogOut,
    handleUpdateUserInfo,
    handleUpdateUserPassword,
    userData,
    firebase,
    userContexts,
  } = props;
  // console.log('rerender');
  // useEffect(() => {
  //   firebase.updateUserBillingAddress(userData.uid, {
  //     orders: [
  //       {
  //         date: 'date',
  //         productName: 'prodcutname',
  //         shipTo: 'shipto1',
  //         saleAmount: 'saleAmount',
  //         status: 'status',
  //       },
  //     ],
  //   });
  // }, []);
  const [pos, setPos] = useState(0);
  const [billingAddressInfo, setBillingAddressInfo] = useState(
    userData.billingAddressInfo
      ? userData.billingAddressInfo
      : {
          billingFullName: 'default name',
          billingAddress: 'default address',
          billingPhone: 'default phone',
          billingEmail: userData.email,
        }
  );

  const [activeMenu, setActiveMenu] = useState('Account Details');
  const [isEditingBillingAddress, setIsEditingBillingAddress] = useState(true);
  const orders = userData.orders ? userData.orders : [];
  const listMenu = ['Account Details', 'Addresses', 'Orders', 'Sign Out'];
  const menu = listMenu.map((item, index) => {
    return (
      <li
        key={index}
        onClick={(e) => {
          if (item === 'Sign Out') {
            handleLogOut();

            return;
          }
          setActiveMenu(e.currentTarget.dataset.name);
        }}
        data-name={item}
        className={`checkout-leftmenu${activeMenu === item ? '--active' : null}`}
      >
        {item}
      </li>
    );
  });
  const OrdersTab = (
    <>
      <table className="table-cart">
        <thead>
          <tr>
            <th>Date</th>
            <th>Product Name</th>
            <th>Amount</th>

            <th>Sale Amount</th>
            <th>Ship To</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {orders.length > 0 ? (
            orders.map((order, index) => {
              if (index < pos * 5 || index >= pos * 5 + 5) {
                console.log(pos * 5, pos * 5 + 5, index);
                return null;
              }
              return (
                <tr key={index}>
                  <td className="product-col">{order.date}</td>
                  <td className="price-col">{order.productName}</td>
                  <td className="price-col">{order.amount}</td>

                  <td className="price-col">{order.saleAmount}</td>
                  <td className="product-col">{order.shipTo}</td>
                  <td className="price-col">{order.status}</td>
                </tr>
              );
            })
          ) : (
            <tr>
              <td className="price-col">
                {' '}
                No Orders.
                <Link to="/sidebar-list">
                  <h4 style={{ margin: '0', display: 'inline' }}> Orders now</h4>
                </Link>
              </td>
              <td />
              <td />
              <td />
              <td />
              <td />
            </tr>
          )}
        </tbody>
      </table>
      <Pagination count={orders.length} pos={pos} setPos={setPos} />
    </>
  );
  const reGetData = async () => {
    const userInfo = await firebase.getUserInfo(userData.uid);
    localStorage.setItem('userInfo', JSON.stringify(userInfo)); // store data
    userContexts.userDispatch({ type: 'SET', payload: userInfo });
  };
  const FormEditBillingAddress = (
    <Formik
      initialValues={billingAddressInfo}
      validate={(values) => {
        const errors = {};

        if (!values.billingAddress) {
          errors.billingAddress = 'Required';
        } else if (values.billingAddress.length < 20) {
          errors.billingAddress = 'Must be 3 characters or more';
        }

        if (!values.billingFullName) {
          errors.billingFullName = 'Required';
        } else if (values.billingFullName.length < 13) {
          errors.billingFullName = 'Must be 3 characters or more';
        }

        if (!values.billingPhone) {
          errors.billingPhone = 'Required';
        } else if (
          !/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(values.billingPhone)
        ) {
          errors.billingPhone = 'Invalid phone number';
        }

        return errors;
      }}
      onSubmit={async (values, { setSubmitting }) => {
        setBillingAddressInfo({ ...billingAddressInfo, ...values });
        firebase.updateUserBillingAddress(userData.uid, { billingAddressInfo: values });

        // reget data
        reGetData();
        setIsEditingBillingAddress(true);
      }}
    >
      {({ values, errors, touched, handleChange, handleBlur, handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <Reveal effect="fadeInUp">
            <div className="content-billing-wrapper">
              <div className="grid wide">
                <div className="row">
                  <label className="account-label">Full Name *</label>
                  <input
                    type="text"
                    className="form-control-input"
                    id="billingFullName"
                    name="billingFullName"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.billingFullName}
                  />
                  <small className="form-text">
                    {errors.billingFullName && touched.billingFullName && errors.billingFullName}
                  </small>
                </div>
                <div className="row">
                  <label className="account-label">Address *</label>
                  <input
                    type="text"
                    className="form-control-input"
                    id="billingAddress"
                    name="billingAddress"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.billingAddress}
                  />
                  <small className="form-text">
                    {errors.billingAddress && touched.billingAddress && errors.billingAddress}
                  </small>
                </div>
                <div className="row">
                  <label className="account-label">Phone Number *</label>
                  <input
                    type="phone"
                    className="form-control-input"
                    id="billingPhone"
                    name="billingPhone"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values.billingPhone}
                  />
                  <small className="form-text">
                    {errors.billingPhone && touched.billingPhone && errors.billingPhone}
                  </small>
                </div>
                <div className="row">
                  <label className="account-label">Email *</label>
                  <input
                    type="email"
                    className="form-control-input"
                    id="billingEmail"
                    name="billingEmail"
                    value={userData.email}
                  />
                </div>
                <button type="submit">
                  Save <i className="fal fa-save" />{' '}
                </button>
              </div>
            </div>
          </Reveal>
        </form>
      )}
    </Formik>
  );
  const billingAddressContent = isEditingBillingAddress ? (
    <>
      <p>
        {billingAddressInfo.billingFullName}
        <br />
        {billingAddressInfo.billingAddress}
        <br />
        {billingAddressInfo.billingPhone}
        <br />
        {billingAddressInfo.billingEmail}
        <br />
      </p>
      <span
        onClick={() => {
          setIsEditingBillingAddress(false);
        }}
      >
        Edit <i className="fal fa-edit" />
      </span>
    </>
  ) : (
    FormEditBillingAddress
  );
  const AddressesTab = (
    <>
      <div className="grid wide">
        <div className="row">
          The following addresses will be used on the checkout page by default.
        </div>
        <div className="row">
          <div className="col l-6 m-6 c-12">
            <div className="box-address">
              <h3 className="cart-overview__heading">Billing Address</h3>
              {billingAddressContent}
            </div>
          </div>
          <div className="col l-6 m-6 c-12">
            <div className="box-address">
              <h3 className="cart-overview__heading">Shipping Address</h3>

              <p>You have not set up this type of address yet.</p>
              <span onClick={() => console.log('handle edit')}>
                Edit <i className="fal fa-edit" />
              </span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
  const AccountDetailTab = (
    <>
      <Formik
        initialValues={{
          firstName: userData ? userData.firstName : '',
          lastName: userData ? userData.lastName : '',
          displayName: userData ? userData.displayName : '',
          currentPassword: '',
          newPassword: '',
          confirmPassword: '',
        }}
        validate={validate}
        onSubmit={(values, { setSubmitting }) => {
          const data2Update = { ...values };
          delete data2Update.currentPassword;
          delete data2Update.newPassword;
          delete data2Update.confirmPassword;

          handleUpdateUserInfo(userData.uid, data2Update);

          if (values.currentPassword) {
            handleUpdateUserPassword(values.newPassword);
          }
          setSubmitting(false);
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          /* and other goodies */
        }) => (
          <form onSubmit={handleSubmit}>
            <div className="row">
              <div className="col l-6 c-12">
                <label className="account-label">First Name *</label>
                <input
                  type="text"
                  className={`form-control-input ${errors.firstName ? '--input-error' : null}`}
                  id="firstName"
                  name="firstName"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.firstName}
                />
                <small className="form-text">
                  {errors.firstName && touched.firstName && errors.firstName}
                </small>
              </div>
              <div className="col l-6 c-12">
                <label className="account-label">Last Name *</label>
                <input
                  type="text"
                  className={`form-control-input ${errors.lastName ? '--input-error' : null}`}
                  id="lastName"
                  name="lastName"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.lastName}
                />
                <small className="form-text">
                  {errors.lastName && touched.lastName && errors.lastName}
                </small>
              </div>
            </div>
            <div className="row">
              <div className="col l-12">
                <label className="account-label">Display Name *</label>
                <input
                  type="text"
                  className={`form-control-input ${errors.displayName ? '--input-error' : null}`}
                  id="displayName"
                  name="displayName"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.displayName}
                />
                <small className="form-text">
                  {errors.displayName && touched.displayName && errors.displayName}
                </small>
                <small className="form-text">
                  This will be how your name will be displayed in the account section and in reviews
                </small>
              </div>
            </div>
            <div className="row">
              <div className="col l-12 c-12">
                <label className="account-label">Email*</label>
                <input
                  type="email"
                  className="form-control-input"
                  defaultValue={userData.email}
                  disabled
                />
              </div>
            </div>

            <div className="row">
              <div className="col l-12 c-12">
                <label className="account-label">
                  Current password (leave blank to leave unchanged)
                </label>
                <input
                  type="password"
                  className={`form-control-input ${
                    errors.currentPassword ? '--input-error' : null
                  }`}
                  id="currentPassword"
                  name="currentPassword"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.currentPassword}
                />
                <small className="form-text">
                  {errors.currentPassword && touched.currentPassword && errors.currentPassword}{' '}
                </small>
              </div>
            </div>

            <div className="row">
              <div className="col l-12 c-12">
                <label className="account-label">
                  New password (leave blank to leave unchanged)
                </label>
                <input
                  type="password"
                  className={`form-control-input ${errors.newPassword ? '--input-error' : null}`}
                  name="newPassword"
                  id="newPassword"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.newPassword}
                />
                <small className="form-text">
                  {errors.newPassword && touched.newPassword && errors.newPassword}
                </small>
              </div>
            </div>
            <div className="row">
              <div className="col l-12 c-12">
                <label className="account-label">Confirm new password</label>
                <input
                  type="password"
                  className={`form-control-input ${
                    errors.confirmPassword ? '--input-error' : null
                  }`}
                  id="confirmPassword"
                  name="confirmPassword"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.confirmPassword}
                />
                <small className="form-text">
                  {errors.confirmPassword && touched.confirmPassword && errors.confirmPassword}
                </small>

                <button
                  type="submit"
                  className="carousel__btn btn-outline-orange checkout-btn"
                  disabled={isSubmitting}
                  value="submit"
                >
                  SAVE CHANGES
                  <i className="fa fa-caret-right" />
                </button>
              </div>
            </div>
          </form>
        )}
      </Formik>
    </>
  );

  const listTab = {
    'Account Details': AccountDetailTab,
    Addresses: AddressesTab,
    Orders: OrdersTab,
  };
  return (
    <div className="checkout-containter">
      <div className="page-title-banner">
        <h3 className="page-title-banner___title">My Account</h3>
        <span className="page-title-banner___subtitle">SHOP</span>
      </div>

      <div className="checkout-content-wrapper">
        <div className="grid wide">
          <div className="row">
            <div className="col l-3 m-6 c-12">
              <div className="checkout-leftmenu">
                <ul>{menu}</ul>
              </div>
            </div>
            <div className="col l-9 m-6 c-12">
              <div className="checkout-right">{listTab[activeMenu]}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
  // else {
  //   props.history.push("/");
  //   showNotification("success", "Please go to account page by sidebar!");
  //   return null;
  // }
};

export default MainAccount;
