import React from 'react';
import { Link } from 'react-router-dom';
import { CSSTransition } from 'react-transition-group';
import * as uiActions from '../../actions/ui';
import { convert2SmallImg } from '../../utils';

const ModalViewCart = (props) => {
  const { uiContexts, cartContexts } = props;
  const item2Show =
    cartContexts.cart.length > 0
      ? cartContexts.cart[cartContexts.cart.length - 1].doc_data
      : {
          description:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
          shortDesc:
            'Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,',
          pictures: [
            'assets/images/demos/demo-2/products/product-10-1-300x300.jpg',
            'assets/images/demos/demo-2/products/product-10-2-300x300.jpg',
          ],
          stock: 10,
          salePrice: 337,
          id: 16,
          new: false,
          featured: false,
          top: true,
          reviews: 11,
          lgPictures: [
            'assets/images/demos/demo-2/products/product-10-1.jpg',
            'assets/images/demos/demo-2/products/product-10-2.jpg',
          ],
          price: 259,
          category: ['Decoration'],
          ratings: 4,
          name: 'Original OutDoor Beanbag',
          brands: ['Nike', 'caprese'],
          demo: ['demo1', 'demo2'],
        };

  const subTotal = () => {
    let sumSubTotal = 0;
    if (cartContexts.cart.length > 0)
      cartContexts.cart.forEach((value, index) => {
        sumSubTotal += value.count * value.doc_data.price;
      });
    return sumSubTotal;
  };
  return (
    <>
      <CSSTransition
        in={uiContexts.ui.isShowModalViewCart}
        classNames="modal-view-cart"
        timeout={300}
      >
        <div className="modal-view-cart">
          <div className="modal-view-cart__container">
            <div
              onClick={() => uiContexts.UiDispatch(uiActions.setModalViewCartStatus(false))}
              className="modal-auth__close-btn"
            >
              <i className="fal fa-times" />
            </div>

            <div className="img-sticky-top">
              <img alt="sticky-image" src={convert2SmallImg(item2Show.pictures[0])} />
            </div>
            <div className="view-cart-info">
              <div className="view-cart-info__heading">{item2Show.name}</div>
              <div className="view-cart-subtotal">
                Cart Subtotal <span>${subTotal()}</span>
              </div>
            </div>
            <div className="modal-view-cart__bottom">
              <div className="continue-shopping-btn">
                <div
                  onClick={() => uiContexts.UiDispatch(uiActions.setModalViewCartStatus(false))}
                  className="carousel__btn btn-outline-orange continue-btn"
                >
                  Continue Shopping
                  <i className="fa fa-caret-right" />
                </div>
              </div>
              <div className="view-cart-btn">
                <Link style={{ color: 'white' }} to="/checkout">
                  <div
                    onClick={() => uiContexts.UiDispatch(uiActions.setModalViewCartStatus(false))}
                    className="carousel__btn btn-outline-orange viewcart-btn"
                  >
                    View Cart <i className="fa fa-caret-right" />
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </CSSTransition>
    </>
  );
};
export default ModalViewCart;
