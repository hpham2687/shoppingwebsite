export const userReducer = (state, action) => {
  switch (action.type) {
    case 'SET':
      return { ...state, authUser: action.payload };
    case 'SET_FETCHING':
      return { ...state, isFetching: action.payload };

    default:
      return state;
  }
};
