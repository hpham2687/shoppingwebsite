import React, { Fragment, useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { Element } from 'react-scroll';
import { convert2SmallImg, numberWithCommas, isInWishlish } from '../../utils';
import Pagination from './Pagination';

const MainSidebarList = (props) => {
  console.log('rerender');
  const data = props.productDataContext.productData;
  const { pos, setPos, handleAddToWishList } = props;
  const [, forceUpdate] = useState(0);

  const { handleAddToCart, uiContexts } = props;
  const { priceValue } = props.filterOptions;
  const filterCategoryOptions = [...props.filterOptions.category];

  useEffect(() => {
    // fix , thao tac left bar thi reset setPost =0
    setPos(0);
  }, [data]);

  if (!data) return <Redirect to="/" />;
  const isInArrayCategory = (ele) => {
    return filterCategoryOptions.includes(ele);
  };

  const checkInCategory = (category, element) => {
    let isExist = false;
    category.forEach((ele) => {
      if (isInArrayCategory(ele)) isExist = true;
    });

    return isExist;
  };

  const RenderStar = ({ numStar }) => {
    return [...Array(5)].map((value, index) => (
      <i key={index} className={`fa fa-star ${index + 1 <= numStar ? 'active-star' : null}`} />
    ));
  };
  const filterData = (data) => {
    if (filterCategoryOptions.length < 1) return [];
    return data
      .filter((element) => {
        const { category } = element.doc_data;
        return checkInCategory(category, element);
      })
      .filter((element) => priceValue >= element.doc_data.price);
  };

  const renderListProducts = filterData(data).map((element, index) => {
    if (index < pos * 5 || index >= pos * 5 + 5) {
      console.log(pos * 5, pos * 5 + 5, index);
      return null;
    }
    const { doc_data } = element;
    const { doc_id } = element;
    return (
      <Fragment key={index}>
        <div className="filterd-product__item">
          <div className="grid">
            <div className="row">
              <div className="col l-3 c-6">
                <div className="thumbnail-image">
                  <img src={convert2SmallImg(doc_data.smPictures[0])} alt="image" />
                </div>
              </div>
              <div id="warapper-description" className="col l-6 c-12">
                <div className="short-description">
                  <h4 className="short-description-category">{doc_data.category.join(', ')}</h4>
                  <h3 className="short-description-name">{doc_data.name}</h3>
                  <p className="description">{doc_data.shortDesc}</p>
                </div>
              </div>

              <div id="wrapper-price" className="col l-3 c-5">
                <div className="price-and-action">
                  <h3>
                    {uiContexts.ui.currency === 'usd'
                      ? `$${doc_data.price}`
                      : `${numberWithCommas(doc_data.price * 22000)} đồng`}
                  </h3>
                  <div
                    onClick={() => {
                      handleAddToWishList({ doc_id, doc_data });
                      forceUpdate((n) => !n);
                    }}
                    className="add-to-wishlist-btn"
                    title="Add to wishlist"
                  >
                    <i className={`${isInWishlish(doc_id) ? 'fas' : 'far'} fa-heart`} />
                    <span style={{ marginLeft: '4px' }}>wishlist</span>
                  </div>
                  <div className="star">
                    <RenderStar numStar={doc_data.ratings} />
                    <span className="num-review">({doc_data.reviews} Reviews)</span>
                  </div>
                  <div
                    onClick={() => handleAddToCart({ doc_id, doc_data })}
                    style={{ fontSize: '1.2rem' }}
                    className="bot"
                  >
                    <i style={{ marginRight: '10px' }} className="far fa-cart-plus" /> Add to cart
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  });

  const unit = 5;
  const pageCount =
    parseInt(filterData(data).length / unit) + (filterData(data).length % unit > 0 ? 1 : 0);

  const numCurrentShowing = () => {
    let currentShowing = 0;
    if (pos === pageCount - 1) {
      currentShowing = filterData(data).length - (5 * (pos - 1) + 5);
    } else if (filterData(data).length < 5) {
      currentShowing = filterData(data).length;
    } else {
      currentShowing = 5;
    }
    return currentShowing;
  };
  return (
    <>
      <Element name="myScrollToListProduct">
        <div className="filterd-product__header">
          <div className="num-of-showing">
            Showing <b>{numCurrentShowing()}</b> of <b>{filterData(data).length}</b> Products
          </div>
          <div className="sort-by-dropdown-menu">
            Sort by:
            <div className="select-custom">
              <select name="sortby" id="sortby" className="form-control">
                <option value="popularity">Most Popular</option>
                <option value="rating">Most Rated</option>
                <option value="date">Date</option>
              </select>
            </div>
          </div>
        </div>
        {renderListProducts}
        <Pagination count={filterData(data).length} pos={pos} setPos={setPos} />
      </Element>
    </>
  );
};

export default React.memo(MainSidebarList);
