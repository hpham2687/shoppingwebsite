import React from 'react';
import CheckoutContainer from '../containers/CheckoutContainer';

const CheckoutPage = (props) => {
  return (
    <>
      <CheckoutContainer {...props} />
    </>
  );
};

export default CheckoutPage;
