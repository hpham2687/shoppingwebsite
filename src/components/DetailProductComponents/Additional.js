import React, { useState } from 'react';
import Tabs from './Tabs';

const Additional = (props) => {
  const { choosingProduct, userContexts } = props;
  const [starPointing, setStarPointing] = useState(1);

  const [review2Send, setReview2Send] = useState({
    rating: null,
    comments: 'deault comment',
  });

  const renderStarReview = () => {
    const arr = new Array(1, 2, 3, 4, 5);
    return arr.map((star) => {
      return (
        <i
          key={star}
          id={star}
          data-id={star}
          onMouseEnter={(e) => {
            // if (review2Send.rating == null)
            setStarPointing(e.currentTarget.dataset.id);
          }}
          onMouseLeave={() => {
            if (review2Send.rating == null) setStarPointing(1);
            else {
              setStarPointing(review2Send.rating);
            }
          }}
          onClick={(e) => {
            const { id } = e.currentTarget.dataset;
            setReview2Send((prevState) => ({ ...prevState, rating: id }));
            setStarPointing(e.currentTarget.dataset.id);
          }}
          className={`fa fa-star ${star <= starPointing ? `active-star` : null}`}
        />
      );
    });
  };
  const RenderStar = ({ numStar }) => {
    return [...Array(5)].map((value, index) => (
      <i key={index} className={`fa fa-star ${index + 1 <= numStar ? 'active-star' : null}`} />
    ));
  };
  const renderTabs = () => {
    return (
      <>
        <Tabs>
          <div className="description-tab" label="Description">
            <h3>Product Information</h3>
            <p>{choosingProduct ? choosingProduct.doc_data.shortDesc : 'Loading'}</p>
          </div>
          <div className="description-tab" label="Additional">
            <h3>Information</h3>
            <p>{choosingProduct ? choosingProduct.doc_data.shortDesc : 'Loading'}</p>
          </div>

          <div className="review-tab" label="Reviews">
            {choosingProduct
              ? choosingProduct.doc_data.comments
                ? choosingProduct.doc_data.comments.map((comment, index) => {
                    return (
                      <>
                        <div className="review-item">
                          <div className="review-item__left">
                            <div className="review-item__name tab-heading ">{comment.name}</div>
                            <div className="star">
                              <RenderStar numStar={comment.rating} />
                            </div>
                          </div>
                          <div className="review-item__right">
                            <div className="brief-rev tab-heading ">Good, perfect size</div>
                            <div className="content-rev">{comment.comments}</div>
                          </div>
                        </div>
                      </>
                    );
                  })
                : 'loadingcmt'
              : 'No review yet'}
            <hr />
            <div className="review-comments">
              <h3 className="review-title">Write your comment</h3>
              <div className="review-quality">
                <div className="star">{renderStarReview()}</div>
              </div>
              <div className="review-content">
                <h3 className="review-content__title">Your comment</h3>
                <textarea
                  onChange={(e) => {
                    const comment = e.target.value;
                    setReview2Send((prevState) => ({
                      ...prevState,
                      comments: comment,
                    }));
                  }}
                  className="review-content__desc"
                  placeholder="Write your comment here..."
                  cols="30"
                  rows="4"
                />
              </div>
              <button
                onClick={() => {
                  props.firebase
                    .updateProductComment(choosingProduct.doc_id, {
                      ...review2Send,
                      name: userContexts.user.authUser.displayName
                        ? userContexts.user.authUser.displayName
                        : 'Guest',
                      when: new Date(),
                    })
                    .then(function () {
                      console.log('Document successfully updated!');
                    })
                    .catch(function (error) {
                      // The document probably doesn't exist.
                      console.error('Error updating document: ', error);
                    });

                  props.fetchProduct();
                }}
                className="btn btn-submit"
              >
                Submit
              </button>
            </div>
          </div>
        </Tabs>
      </>
    );
  };
  return (
    <>
      <div className="row">
        <div className="col l-12">{renderTabs()}</div>
      </div>
    </>
  );
};

export default Additional;
