import React from 'react';
import { useTranslation } from 'react-i18next';

const OurFeatures = () => {
  const { t, i18n } = useTranslation();

  const listFeatures = [
    {
      title: t('below_arrival.payment_title'),
      content: t('below_arrival.payment_content'),
    },
    {
      title: t('below_arrival.return_title'),
      content: t('below_arrival.return_content'),
    },
    {
      title: t('below_arrival.customer_title'),
      content: t('below_arrival.customer_content'),
    },
  ];
  return (
    <>
      <div className="our-features">
        <div className="grid wide">
          <div className="row">
            {listFeatures.map((item, index) => {
              return (
                <div className="col l-4 m-4 c-12">
                  <div className="our-features-item">
                    <h3 className="our-features-item__title">
                      <i className="far fa-rocket" />
                      <br />
                      {item.title}
                    </h3>
                    <span className="our-features-item__description">{item.content}</span>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
};

export default OurFeatures;
