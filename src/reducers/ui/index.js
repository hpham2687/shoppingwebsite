export const uiReducer = (state, action) => {
  switch (action.type) {
    case 'SET_MODAL_SIDEBAR':
      return { ...state, isShowModalSidebar: action.payload };
    case 'SET_MODAL_RESET_PASSWORD':
      return { ...state, isShowModalResetPassword: action.payload };
    case 'SET_MODAL_AUTH_STATUS':
      return { ...state, isShowModalAuth: action.payload };
    case 'SET_MODAL_VIEW_CART_STATUS':
      return { ...state, isShowModalViewCart: action.payload };
    case 'SET_TRENDY_PRODUCT_TAB':
      return { ...state, trendyProductTab: action.payload };
    case 'SET_RECENT_ARRIVAL_PRODUCT_TAB':
      return { ...state, recentArrivalProductTab: action.payload };
    case 'SET_CURRENCY':
      return { ...state, currency: action.payload };

    default:
      return state;
  }
};
