import React, { useContext, useEffect, useState } from 'react';
import * as ContextApi from '../context';
import SubDetailProduct from '../components/DetailProductComponents/SubDetailProduct';
import Firebase from '../firebase';
import Additional from '../components/DetailProductComponents/Additional';

const DetailProductContainer = (props) => {
  const idProduct = props.match.params.id;

  const { ProductDataContexts, UiContexts, CartContexts, UserContexts } = ContextApi;
  const productDataContext = useContext(ProductDataContexts);
  const uiContexts = useContext(UiContexts);
  const userContexts = useContext(UserContexts);
  const cartContexts = useContext(CartContexts);

  // console.log(userContexts);

  const [choosingProduct, setChoosingProduct] = useState(null);
  const fetchProduct = async () => {
    let product = null;
    await Firebase.getAllItems('productsData').then(async (querySnapshot) => {
      await querySnapshot.forEach((value, index) => {
        if (value.id === idProduct)
          product = {
            doc_id: value.id,
            doc_data: value.data(),
          };
      });

      setChoosingProduct(product);
    });
  };
  useEffect(() => {
    fetchProduct();
  }, []);
  return (
    <>
      <div className="detail-page">
        <div className="grid wide">
          <SubDetailProduct
            uiContexts={uiContexts}
            cartContexts={cartContexts}
            productDataContext={productDataContext}
            choosingProduct={choosingProduct}
            userContexts={userContexts}
            firebase={Firebase}
            {...props}
          />
          <Additional
            choosingProduct={choosingProduct}
            fetchProduct={fetchProduct}
            firebase={Firebase}
            userContexts={userContexts}
          />
        </div>
      </div>
    </>
  );
};

export default DetailProductContainer;
